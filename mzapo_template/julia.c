/*******************************************************************
  Simple program to check LCD functionality on MicroZed
  based MZ_APO board designed by Petr Porazil at PiKRON

  mzapo_lcdtest.c       - main and only file

  (C) Copyright 2004 - 2017 by Pavel Pisa
      e-mail:   pisa@cmp.felk.cvut.cz
      homepage: http://cmp.felk.cvut.cz/~pisa
      work:     http://www.pikron.com/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

#define HEIGHT 320
#define WIDTH 480


/*thread structs*/
pthread_t pool[4];
/*mutex struct*/
pthread_mutex_t lock;

uint8_t red_color, green_color, blue_color; //colours variables

uint8_t red, green, blue;  // moving knobs variables

uint8_t red_old, green_old, blue_old = 0;  //moving variables of the last loop

double cRe, cIm;           //real and imaginary part , determinate shape of the Julia Set

double zoom = 1, moveX = 0, moveY = 0; //you can change these to zoom and change position


int end = 1;

uint32_t knobs;

unsigned char *mem_base;

unsigned char *parlcd_mem_base;



//reading variables from the knobs
uint32_t get_knobs(unsigned char *mem_base) {
    if (mem_base == NULL) {
        exit(1);
    }
    uint32_t knobs;
    knobs = *(volatile uint32_t *) (mem_base + SPILED_REG_KNOBS_8BIT_o);
    return knobs;
}

// change of the colour
void change_color(uint32_t knobs) {
    int bits[32];
    for (int i = 31; i >= 0; i--) { bits[i] = (knobs & (1 << i)) != 0; }
    int red_but = bits[26];
    int green_but = bits[25];
    int blue_but = bits[24];

    int color_increment = 3;

    if (red_but == 1) {
        red_color += color_increment;
        if (red_color == 0) red_color = 1;
    }

    if (green_but == 1) {
        green_color += color_increment;
        if (green_color == 0) green_color = 1;
    }

    if (blue_but == 1) {
        blue_color += color_increment;
        if (blue_color == 0) blue_color = 1;
    }

}

//change zoom, real and imaginary part
void change_parameters(uint32_t knobs) {

    blue = knobs & 0xFF;
    green = (knobs >> 8) & 0xFF;
    red = knobs >> 16;


    if (red > red_old) { zoom += 0.1; }
    if (red < red_old) { zoom -= 0.1; }

    if (green > green_old) { cRe = cRe - 0.1; }
    if (green < green_old) { cRe = cRe + 0.1; }

    if (blue > blue_old) { cIm = cIm + 0.01; }
    if (blue < blue_old) { cIm = cIm - 0.01; }


}


// compute the Julia set formule, get colour of each picture and send it to the lcd screen
void compute_and_send_data(unsigned char *parlcd_mem_base) {


    u_int16_t color;


    //each iteration, it calculates: new = old*old + c, where c is a constant and old starts at current pixel

    double newRe, newIm, oldRe, oldIm;   //real and imaginary parts of new and old

    //ColorRGB color; //the RGB color value for the pixel
    int maxIterations = 300; //after how much iterations the function should stop



    int h = 320;
    int w = 480;

    //printf("pew \n");




            //loop through every pixel
    for (int y = 0; y < HEIGHT; y++)
        for (int x = 0; x < WIDTH; x++) {
            //calculate the initial real and imaginary part of z, based on the pixel location and zoom and position values
            newRe = 1.5 * (x - w / 2) / (0.5 * zoom * w) + moveX;
            newIm = (y - h / 2) / (0.5 * zoom * h) + moveY;
            //i will represent the number of iterations
            int i;
            //start the iteration process
            for (i = 0; i < maxIterations; i++) {
                //remember value of previous iteration
                oldRe = newRe;
                oldIm = newIm;
                //the actual iteration, the real and imaginary part are calculated
                newRe = oldRe * oldRe - oldIm * oldIm + cRe;
                newIm = 2 * oldRe * oldIm + cIm;
                //if the point is outside the circle with radius 2: stop
                if ((newRe * newRe + newIm * newIm) > 4) break;
            }

            //draw the pixel






            color = (i % red_color * green_color * blue_color * (i < maxIterations));

            parlcd_write_data(parlcd_mem_base, color);

        }


    //printf("pie \n");

    red_old = red;
    green_old = green;
    blue_old = blue;


}



/*
 *
 * TRYING TO DO SOMITHING WITH THE THREADS
 *
 */
int do_knobs, do_color, do_parameters, do_lcd_1, do_lcd_2 = 0;


void *get_knobs_thread(void *arg) {
    printf("1 \n");
    while (end) {
        if (do_knobs) {

            knobs = get_knobs(mem_base);
            do_knobs = 0;
            do_color=1;

            printf("%s \n", arg);
            pthread_join(arg,pool[2]);

        }
    }
}


void *change_color_thread(void *arg) {
    printf("2 \n");
    while (end) {
        if (do_color) {

            change_color(knobs);
            do_color = 0;
            do_parameters=1;
            pthread_join(arg,pool[3]);

        }
    }
}


void *change_parameters_thread(void *arg) {
    printf("3 \n");
    while (end) {
        if (do_parameters) {

            change_parameters(knobs);
            do_parameters = 0;
            do_lcd_1=1;
            pthread_join(arg,pool[0]);

        }
    }
}


void *lcd_thread(void *arg) {
    printf("4 \n");


        while(1) {

            pthread_mutex_lock(&lock);
            if (do_lcd_1) {
                //pthread_mutex_lock(&lock);
                //printf("start \n");

                compute_and_send_data(parlcd_mem_base);

                do_lcd_1 = 0;

                //pthread_mutex_unlock(&lock);
                //do_knobs = 1;

//                pthread_join(arg,pool[1]);
//
//                printf("end \n");






                //do_knobs = 1;

            }
            else {
                knobs = get_knobs(mem_base);




                change_color(knobs);

                change_parameters(knobs);

                do_lcd_1=1;
            }

            pthread_mutex_unlock(&lock);

        }


    return NULL;



        //printf("end2 \n");



}

// Some animation on the start
void animation(){
    int a=0;
    while (a<50){

        a++;
        red_color+=a;
        green_color+=a%10;
        blue_color-=a/10;
        zoom-=0.01;
        cRe-=0.01;
        cIm+=0.01;

        compute_and_send_data(parlcd_mem_base);

        uint32_t knobs;
        knobs = *(volatile uint32_t *) (mem_base + SPILED_REG_KNOBS_8BIT_o);

        int bits[32];

        for (int i = 31; i >= 0; i--) { bits[i] = (knobs & (1 << i)) != 0; }
        int red_but = bits[26];



        if (red_but == 1) {
            break;
        }




        parlcd_delay(400);


    }


    cRe = -0.7;
    cIm = 0.27015;
    zoom=1;


    red_color = 30;
    green_color = 120;
    blue_color = 60;
}




int main(int argc, char *argv[]) {




    //set some values
    cRe = -0.7;
    cIm = 0.27015;


    red_color = 30;
    green_color = 120;
    blue_color = 60;


    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);


    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

    if (parlcd_mem_base == NULL)
        exit(1);

    parlcd_hx8357_init(parlcd_mem_base);

    parlcd_write_cmd(parlcd_mem_base, 0x2c);

    animation();

//    if (pthread_mutex_init(&lock, NULL) != 0) {
//        printf("Mutex init error!\n");
//        return 1;
//    }
//    if ((pthread_create(&(pool[0]), NULL, &get_knobs_thread, NULL)) != 0) {
//        printf("Error creating knobs thread!");
//        return 1;
//    }
//    if ((pthread_create(&(pool[1]), NULL, &change_color_thread, NULL)) != 0) {
//        printf("Error creating color thread!");
//        pthread_cancel(pool[0]);
//        return 1;
//    }
//
//    if ((pthread_create(&(pool[2]), NULL, &change_parameters_thread, NULL)) != 0) {
//        printf("Error creating parameters thread!");
//        pthread_cancel(pool[0]);
//        pthread_cancel(pool[1]);
//        return 1;
//    }

    pthread_t knobs_t,color, parameters, lcd;

//    (pthread_create(&(pool[0]), NULL, get_knobs_thread, NULL));
//    (pthread_create(&(pool[1]), NULL, change_color_thread, NULL));
//    (pthread_create(&(pool[2]), NULL, change_parameters_thread, NULL));
    if((pthread_create(&lcd, NULL, lcd_thread, NULL))!= 0) {
        printf("Error creating lcd thread!");

        return 1;
    }

    do_knobs=1;








    // Get values, change colour and some parameters and then
    // send them to the lcd
    while (1) {


        if(pthread_join(lcd,NULL)!=0) printf("Error join lcd thread!");

        knobs = get_knobs(mem_base);
        change_color(knobs);

        change_parameters(knobs);

        do_lcd_1=1;

        //compute_and_send_data(parlcd_mem_base);




    }






    end = 0;
    pthread_mutex_destroy(&lock);
    pthread_join(pool[0], NULL);
    pthread_join(pool[1], NULL);
    pthread_join(pool[2], NULL);
    pthread_join(pool[3], NULL);


    return 0;
}
